Social API Endpoints
====================
News
----
.. http:get:: http://bigterminal.com/api/v1/social/news

    Outputs news article information on a search query.

    :query q: The search query. ``required``
    :query limit: The number of articles to return ``default=3``

    **Example Request**

    .. sourcecode:: http

        GET /api/v1/social/news?q=AAPL&limit=1 HTTP/1.1
        Host: bigterminal.com
        Accept: application/json

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Date: Thu, 29 Aug 2013 00:00:00 GMT
        Content-Type: application/json; charset=utf-8
        Content-Length: 1735
        Connection: keep-alive
        Vary: Accept-Encoding
        ETag: "-239885810"

        {
            "status": "success",
            "data": [
                {
                    "date": "Wed, 21 Aug 2013 02:28:45 -0700",
                    "link": "http://www.benzinga.com/analyst-ratings/analyst-color/13/08/3851233/apple-acquired-authentec-to-become-the-champion-of-mobil",
                    "hostname": "www.benzinga.com",
                    "title": "Apple Acquired AuthenTec To Become The 'Champion Of Mobile Commerce'",
                    "body": "Apple Acquired AuthenTec To Become The 'Champion Of Mobile Commerce' (AAPL). Louis Bedigian, Benzinga Staff Writer. August 20, 2013 10:44 AM. + Follow. Print. Email. Tickers: AAPL, HPQ. Share: Last year, Apple (NASDAQ: AAPL) made a massive ..."
                }
            ]
        }

Stream
------
.. http:get:: http://bigterminal.com/api/v1/social/stream

    Outputs a social feed containing a search query.

    :query q: The search query. ``required``
    :query providers: A csv list of social providers to query. Supported providers are: ['stocktwits', 'twitter', 'facebook']. `Only stocktwits is enabled at the moment` ``default=stocktwits``
    :query limit: The number of posts to return ``default=9``

    **Example Request**

    .. sourcecode:: http

        GET /api/v1/social/stream?q=AAPL&limit=1 HTTP/1.1
        Host: bigterminal.com
        Accept: application/json

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Date: Thu, 29 Aug 2013 00:00:00 GMT
        Content-Type: application/json; charset=utf-8
        Content-Length: 3810
        Connection: keep-alive
        Vary: Accept-Encoding
        ETag: "962233693"

        {
            "status": "success",
            "data": [
                {
                    "from_user": "ProJabber",
                    "profile_img_ssl": "https://s3.amazonaws.com/st-avatars/production/69296/thumb-1377277243.png",
                    "date": "Thu Aug 29 2013 05:29:03 GMT+0000 (UTC)",
                    "text": "<a target='_blank' href='https://stocktwits.com/symbol/aapl'>$aapl</a> bulls get annoyed when I share my bearish views, but investors in <a target='_blank' href='https://stocktwits.com/symbol/pcln'>$pcln</a> might want to shoot me when I post my analysis before open",
                    "id": 15482414,
                    "type": "stocktwits"
                }
            ]
        }