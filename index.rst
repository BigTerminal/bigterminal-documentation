.. BigTerminal.com API Documentation master file, created by
   sphinx-quickstart on Wed Aug 28 20:40:39 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BigTerminal.com's API Documentation!
=============================================================

API Endpoints:

.. toctree::
   :maxdepth: 2

   data
   social

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`