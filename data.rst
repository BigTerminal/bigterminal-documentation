Data API Endpoints
==================

Data Load
---------
.. http:get:: http://bigterminal.com/api/v1/data-load

    Outputs basic information on the queried symbol(s) and data series for the symbol(s).

    :query key: API access key granted by BigTerminal.com staff. ``required``
    :query seriess: A series or CSV list of series to query. ``required``
    :query tokens: A user-chosen numerical identifier for each series. ``required``
    :query count: The number of output ticks requested for this query. Number of output ticks may not match the count exactly. ``required``
    :query start: The start date timestamp (in seconds) or string (format: %Y-%m-%d %H:%M:%S). ``required``
    :query is_realtime: Boolean. Realtime data and historical data may have different output formats and contain different date ranges. ``required``
    :query end: The end date timestamp (in seconds) or string (format: %Y-%m-%d %H:%M:%S). ``default=current datetime``
    :query denominations: A series or list of series or currencies to reprice the requested series into. ``optional, USD in most cases``
    :query tz: Timezone the results will be output in. ``default=UTC``
    :query resolution: Overwrite the `count` parameter to request a specific standardized resolution ``optional``
    :query units: Scale metal series (type `M`) to different units. Valid units are one of ['gr', 'kg', 'tonnes', 'troy_oz', 'tael', 'tola'] ``default=troy_oz``
    :query scales: A scale or CSV list of scales the output series will be scaled into. Basically divides the observations by `scale` ``default=1``
    :query adjusted: Adjust series for stock splits. A boolean or list of booleans. ``default=False``
    :query update: Used to update realtime data. Outputs ALL non-standardized ticks which occured since `start`. ``default=False``
    :query asc: Boolean. Sort ascending or not (descending). ``default=True``
    :query extra_info: Boolean. Extra information for certain series types. Right now this is outputs Bitcoin difficulty chart data and Bitcoin market status when enabled. ``default=False``

    **Example Request**

    .. sourcecode:: http

        GET /api/v1/data-load?seriess=AAPL,BTCUSD&start=1376973416&end=1377059816&is_realtime=True&count=10&tokens=1,2&tz=US/Eastern&denominations=CAD,USD&key=#### HTTP/1.1
        Host: bigterminal.com
        Accept: application/json

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Date: Thu, 30 Aug 2013 00:00:00 GMT
        Content-Type: application/json
        Content-Length: 740
        Connection: keep-alive
        Content-Encoding: gzip

        {
            "symbols": [
                {
                    "count": 4,
                    "name": "Apple Inc.",
                    "fundamentals": {
                        "Employees": 72800,
                        "ReturnOnAssets": 18.9,
                        "EBITDAPerShare": 33
                    },
                    "symbol": "AAPL",
                    "denomination": "Canadian Dollar",
                    "token": "1",
                    "observations": [
                        {
                            "volume": 228848,
                            "last": 524.7676490604658,
                            "datetime": "2013-08-20 10:00:00"
                        },
                        {...}
                        {
                            "volume": 1064689,
                            "last": 520.7350673853327,
                            "datetime": "2013-08-20 16:00:00"
                        }
                    ],
                    "denomination_as_queried": "CAD",
                    "type": "Q",
                    "id": 26524
                },
                {
                    "count": 12,
                    "name": "Bitcoin to US dollar",
                    "symbol": "BTCUSD",
                    "denomination": "United States Dollar",
                    "token": "2",
                    "observations": [
                        {
                            "volume": 1.29,
                            "price": 120.20306,
                            "datetime": "2013-08-20 06:00:00"
                        },
                        {...}
                        {
                            "volume": 0.01008,
                            "price": 120.76502,
                            "datetime": "2013-08-21 04:00:00"
                        }
                    ],
                    "denomination_as_queried": "USD",
                    "type": "B",
                    "id": 76411
                }
            ],
            "resolution": "2h",
            "result": "success"
        }

Symbols Metadata
----------------
.. http:get:: http://bigterminal.com/api/v1/symbols-md

    Outputs information and metadata on the queried symbol.

    :query key: API access key granted by BigTerminal.com staff. ``required``
    :query seriess: A series or CSV list of series to query. ``required``
    :query denominations: A series or list of series or currencies to reprice the requested series into. ``optional, USD in most cases``
    :query tokens: A user-chosen numerical identifier for the series. ``required``
    :query md_token: The series token which to output `metadata` for. ``required``
    :query tz: Timezone the results will be output in. ``default=UTC``
    :query extra_info: Boolean. Extra information for certain series types. Right now this is outputs Bitcoin difficulty chart data and Bitcoin market status when enabled. ``default=False``

    **Example Request**

    .. sourcecode:: http

        GET /api/symbols-md?md_token=2&tokens=1,2&extra_info=False&seriess=AAPL,BTCUSD&key=#### HTTP/1.1
        Host: bigterminal.com
        Accept: application/json

    **Example Response**

    .. sourcecode:: http

        HTTP/1.0 200 OK
        Content-Type: application/json
        Content-Length: 1055
        Content-Encoding: gzip
        Date: Thu, 29 Aug 2013 00:00:00 GMT

        {
            "market_snapshot": {
                "YM_FM": {
                    "price_percent_change": 0.013520365049856346,
                    "price_change": 2,
                    "last_close": 14792.5,
                    "last_price": 14797,
                    "last_open": 14795
                },
                "NQ_FM": {
                    "price_percent_change": 0.040749796251018745,
                    "price_change": 1.25,
                    "last_close": 3067.5,
                    "last_price": 3069.5,
                    "last_open": 3068.25
                },
                "ES_FM": {
                    "price_percent_change": 0.026188285801901256,
                    "price_change": 0.427,
                    "last_close": 1630.5,
                    "last_price": 1631,
                    "last_open": 1630.573
                }
            },
            "symbols_md": [
                {
                    "industry_ratios": {
                        "PercentageAssetTurnoverToIndustry": 0,
                        "PercentageDebtEquityToIndustry": 0,
                        "PercentagePriceSalesToIndustry": 126.8,
                        "PercentageLeverageRatioToIndustry": 0,
                        "PercentageEarningsToIndustry": 0,
                        "PercentageGrossProfitMarginToIndustry": 0,
                        "PercentagePriceBookToIndustry": 95.4,
                        "PercentagePriceToIndustry": 319.3,
                        "PercentageSalesToIndustry": 0,
                        "PercentagePreTaxProfitMarginToIndustry": 0,
                        "PercentagePriceCashFlowToIndustry": 103,
                        "PercentagePriceFreeCashFlowToIndustry": 215,
                        "PercentageEPSToIndustry": 0,
                        "PercentageReturnOnEquityToIndustry": 0,
                        "PercentagePEToIndustry": 103.4,
                        "PercentagePostTaxProfitMarginToIndustry": 0,
                        "PercentageCurrentRatioToIndustry": 0,
                        "PercentageNetProfitMarginToIndustry": 0
                    },
                    "scale": 1,
                    "splits": [
                        {
                            "cummulated_ratio": 4,
                            "ratio": 2,
                            "datetime": "2012-05-25 20:46:10"
                        },
                        {
                            "cummulated_ratio": 2,
                            "ratio": 2,
                            "datetime": "2012-05-25 20:46:10"
                        }
                    ],
                    "fundamentals": {
                        "Employees": 72800,
                        "ReturnOnAssets": 18.9,
                        "EBITDAPerShare": 33
                    },
                    "negative": 0,
                    "precision": 3,
                    "unit_type": "USD",
                    "token": "1",
                    "category_industry": "CONSUMER GOODS",
                    "id": 26524,
                    "financials": {
                        "NetProfitMargin": 25.3,
                        "ReturnOnStockEquity": 32.8
                    }
                },
                {
                    "scale": 1,
                    "negative": 0,
                    "precision": 5,
                    "unit_type": "CUR",
                    "token": "2",
                    "id": 76411
                }
            ],
            "metadata": {
                "minimum_frequency": "",
                "last_price": 128.89997,
                "5y_high": 265.999,
                "daily_low": 128.18,
                "daily_high": 130.15027,
                "52week_high": 265.999,
                "total_btc": 11511500,
                "52week_volatility": 92.13798177943013,
                "max": 683.586443768997,
                "blocks_per_hour": 7.292,
                "last_open": 130.20501,
                "blocks": 250459,
                "52week_low": 9.66001,
                "symbol": "BTCUSD",
                "difficulty": 65750060.149084814,
                "52week_standard_deviation": 48.3339397370064,
                "last_close": 128.76,
                "1_year_to_date": 1080.737846824648,
                "name": "Bitcoin to US dollar",
                "year_to_date": 865.1278776016903,
                "type": "B",
                "price_change": 0.13997,
                "interval": 493.71428571428567,
                "price_percent_change": 0.10870611991301646,
                "token": "2",
                "next_retarget": 256031,
                "52week_signal_to_noise": 1.0853287435727734,
                "5y_low": 2.50337,
                "hash_rate": 571.9793131807
            }
        }
        